# Active Record
Active Record is the M in MVC - the model - which is the layer of the system responsible for representing business data and logic.
It Represent models and their data.
It Represent associations between these models.

# Naming Conventions
By default, Active Record uses some naming conventions to find out how the mapping between models and database tables should be created. Rails will pluralize your class names to find the respective database table. So, for a class Book, you should have a database table called books.

# CRUD

## Create
```
user = User.create(name: "David", occupation: "Code Artist")
```
or 
```
user = User.new
user.name = "David"
user.occupation = "Code Artist"
```
The new method will return a new object while create will return the object and save it to the database.

## Read
```users = User.all```

```user = User.first```

## Update
```
user = User.find_by(name: 'David')
user.update(name: 'Dave')
```

## Delete

```
user = User.find_by(name: 'David')
user.destroy
```


### find
```customer = Customer.find(10)```

retrieve the object corresponding to the specified primary key that matches any supplied options

### first
The first method finds the first record ordered by primary key (default)
```customer = Customer.first```

### last
The last method finds the last record ordered by primary key (default)
```customer = Customer.last```

### find_by
The find_by method finds the first record matching some conditions.

```Customer.find_by first_name: 'Lifo'```

##  Retrieving Multiple Objects in Batches
    Customer.all.each do |customer|
        NewsMailer.weekly(customer).deliver_now
    end
But this approach becomes increasingly impractical as the table size increases, since Customer.all.each instructs Active Record to fetch the entire table in a single pass

## find_each
The find_each method retrieves records in batches and then yields each one to the block. find_each retrieves customers in batches of 1000 and yields them to the block one by one.

    Customer.find_each do |customer|
        NewsMailer.weekly(customer).deliver_now
    end

## where
The where method allows you to specify conditions to limit the records returned

```Book.where("title = 'Introduction to Algorithms'")```

If you want to find records using the IN expression you can pass an array to the conditions hash

```Customer.where(orders_count: [1,3,5])```

Similarly for range condition

```Book.where(created_at: (Time.now.midnight - 1.day)..Time.now.midnight)```

## order
To retrieve records from the database in a specific order, you can use the order method.

```Customer.order(created_at: :desc)```

## select
By default, Model.find selects all the fields from the result set using select *.

    Book.select(:isbn, :out_of_print)
    # OR
    Book.select("isbn, out_of_print")

    Customer.select(:last_name).distinct

## limit
You can use limit to specify the number of records to be retrieved

```Customer.limit(5)```

## group
To apply a GROUP BY clause to the SQL fired by the finder, you can use the group method.

```Order.select("created_at").group("created_at")```

## count
To get the total of grouped items on a single query, call count after the group.    

```Order.group(:status).count```

## having
SQL uses the HAVING clause to specify conditions on the GROUP BY fields. 
```
Order.select("created_at, sum(total) as total_price").group("created_at")
.having("sum(total) > ?", 200)
```

## Readonly Objects
Readonly method on a relation to explicitly disallow modification of any of the returned objects. Any attempt to alter a readonly record will not succeed, raising an ActiveRecord::ReadOnlyRecord exception.

```
customer = Customer.readonly.first
customer.visits += 1
customer.save
```
## Joins
You can just supply the raw SQL specifying the JOIN clause to joins

```Author.joins("INNER JOIN books ON books.author_id = authors.id AND books.out_of_print = FALSE")```

Similarly for **LEFT OUTER JOIN** we can use ```left_outer_joins```.

## find_or_create_by
The find_or_create_by method checks whether a record with the specified attributes exists. If it doesn't, then create is called.  

```Customer.find_or_create_by(first_name: 'Andy')```

## find_or_initialize_by
The find_or_initialize_by method will work just like find_or_create_by but it will call new instead of create. This means that a new model instance will be created in memory but won't be saved to the database.

## Finding by SQL
If you'd like to use your own SQL to find records in a table you can use find_by_sql.

```Customer.find_by_sql("SELECT * FROM customers INNER JOIN orders ON customers.id = orders.customer_id ORDER BY customers.created_at desc")```

## Existence of Objects
If you simply want to check for the existence of the object there's a method called exists?.

```Customer.exists?(1)```

## Calculations
### count

```Customer.where(first_name: 'Ryan').count```

If you want to see how many records are in your model's table
### Average
```Order.average("subtotal")```
### Minimum
```Order.minimum("subtotal")```
### Maximum
```Order.maximum("subtotal")```
### Sum
```Order.sum("subtotal")```

## Explain
You can run explain on a relation

```Customer.where(id: 1).joins(:orders).explain```